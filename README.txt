sms_plivo
~~~~~~~~~~

sms_plivo is a drupal module to add the plivo.com gateway to the sms framework module.

INSTALL
~~~~~~~

1. Download the plivo bundled PHP library https://github.com/crownemmanuel/plivo_bundled/archive/master.zip and unzip into sites/all/libraries (this path can be edited when setting up the gateway).
   The downloaded library should be in sites/all/libraries/plivo_bundled

2. See the getting started guild on installing drupal modules:
http://drupal.org/getting-started/install-contrib/modules

3. Go to admin/smsframework/gateways to setup the plivo gateway.

Credits
*******
Emmanuel Crown
Alot of concepts borrowed from https://www.drupal.org/project/sms_twilio

LICENSE
~~~~~~~

This software licensed under the GNU General Public License 2.0.
